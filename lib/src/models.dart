import 'package:vanphash/src/types.dart';
import 'package:vanphash/src/utils.dart';

class Password {
  /// Password object for easy password management.
  String _password;

  Password(String password) {
    this._password = password;
  }

  password() {
    /// Retrives the password string.
    return this._password;
  }

  length() {
    /// Retrives the password character count.
    return this._password.length;
  }

  count_chars(CharType type) {
    /// Retrives the password character count by type.
    return this.get_chars(type).length;
  }

  get_chars(CharType type, {bool with_index = false}) {
    /// Retrives the password characters by type.
    var chars = [];
    for (int i = 0; i < this._password.length; i++) {
      if (is_char_type(this._password[i], type)) {
        if (with_index) {
          chars.add([i, this._password[i]]);
        } else {
          chars.add(this._password[i]);
        }
      }
    }

    return chars;
  }
}
