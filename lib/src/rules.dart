import 'package:vanphash/src/types.dart';
import 'package:vanphash/src/utils.dart';
import 'package:vanphash/src/models.dart';
import 'package:vanphash/src/exceptions.dart';

class RuleActions {
  /// Base rules actions object.
  Password _password;
  int _seed = 0;

  RuleActions(String password) {
    this._password = Password(password);
    for (int i = 0; i < password.length; i++) {
      this._seed += password.codeUnitAt(i);
    }
  }

  inject_character(int pos, CharType type) {
    /// Injects random character of type on specific position.
    int char_num = 0;
    String start_char = '';

    if (type == CharType.digit) {
      char_num = 10;
      start_char = '0';
    } else if (type == CharType.lower_alpha) {
      char_num = 26;
      start_char = 'a';
    } else if (type == CharType.upper_alpha) {
      char_num = 26;
      start_char = 'A';
    } else if (type == CharType.special) {
      char_num = 15;
      start_char = '!';
    }
    int start_char_code = start_char.codeUnitAt(0);

    this._password = Password(replace_char(
        this._password.password(), pos, this._seed, char_num, start_char_code));
  }
}

class QuantityRule extends RuleActions {
  /// Quantity rules collection.
  List<int> _protected_chars = List<int>();

  QuantityRule(String password) : super(password);

  size(int length) {
    /// Rule to trim password to specific size.
    this._password = Password(this._password.password().substring(0, length));
  }

  min_chars(int min_count, CharType type) {
    /// Rule to verify that the password has minimum specified number of
    /// characters by type.
    if (this._password.length() - this._protected_chars.length < min_count) {
      throw IllegalRule('Not enough characters on password to apply rule.');
    }
    if (this._password.count_chars(type) < min_count) {
      var actual_chars = this._password.get_chars(type, with_index: true);
      for (int i = 0; i < actual_chars.length; i++) {
        if (!this._protected_chars.contains(i)) {
          this._protected_chars.add(actual_chars[i][0]);
        }
      }
      int index = 0;
      for (int i = 0; i < this._password.length(); i++) {
        if (this._password.count_chars(type) >= min_count) {
          break;
        }
        if (index + i > this._password.length()) {
          index -= this._password.length();
        }
        if (!is_char_type(this._password.password()[i], type) &&
            !this._protected_chars.contains(i) &&
            index % 4 != 0) {
          this._protected_chars.add(i);
          this.inject_character(i, type);
        }
        index++;
      }
    }
  }

  get_password() {
    /// Retrieves password object
    return this._password;
  }
}
