class IllegalRule implements Exception {
  /// Exception to be launched when a rule can be applied.
  String cause;
  IllegalRule(this.cause);
}
