import 'package:vanphash/src/types.dart';

is_char_type(var char, CharType type) {
  /// Validates that character is of certain type.
  if (char == null) {
    return false;
  }

  var regex;

  if (type == CharType.digit) {
    regex = RegExp(r'^[0-9]+$');
  } else if (type == CharType.lower_alpha) {
    regex = RegExp(r'^[a-z]+$');
  } else if (type == CharType.upper_alpha) {
    regex = RegExp(r'^[A-Z]+$');
  } else if (type == CharType.special) {
    regex = RegExp(r'[^\s\w]');
  }

  return regex.hasMatch(char);
}

convert_to_digits(String input, int seed, int length) {
  // Converts chars to digits
  int pivot = 0;
  int zeroChar = '0'.codeUnitAt(0);
  for (int i = 0; i < length; i++) {
    if (!is_char_type(input[i], CharType.digit)) {
      String digit =
          String.fromCharCode((seed + input.codeUnitAt(pivot)) % 10 + zeroChar);
      input = input.substring(0, i) + digit + input.substring(i + 1);
      pivot = i + 1;
    }
  }

  return input;
}

remove_special_characters(String input, int seed, int length) {
  // Removes special characters
  int pivot = 0;
  int aChar = 'A'.codeUnitAt(0);
  for (int i = 0; i < length; i++) {
    if (!is_char_type(input[i], CharType.digit) &&
        !is_char_type(input[i], CharType.lower_alpha) &&
        !is_char_type(input[i], CharType.upper_alpha)) {
      var char = String.fromCharCode((seed + pivot) % 26 + aChar);
      input = input.substring(0, i) + char + input.substring(i + 1);
      pivot = i + 1;
    }
  }

  return input;
}

replace_char(
    String input, int index, int seed, int char_num, int start_char_code) {
  /// Replaces character for random one
  String head = index > 0 ? input.substring(0, index) : '';
  String inject = String.fromCharCode(
      ((seed + input.codeUnitAt(index)) % char_num) + start_char_code);
  String tail = (index + 1 < input.length) ? input.substring(index + 1) : '';

  return head + inject + tail;
}

random_inject_character(String input, int offset, int reserved, int seed,
    int length, CharType type) {
  /// Twik random character injection.
  int char_num = 0;
  String start_char = '';

  if (type == CharType.digit) {
    char_num = 10;
    start_char = '0';
  } else if (type == CharType.lower_alpha) {
    char_num = 26;
    start_char = 'a';
  } else if (type == CharType.upper_alpha) {
    char_num = 26;
    start_char = 'A';
  } else if (type == CharType.special) {
    char_num = 15;
    start_char = '!';
  }
  int start_char_code = start_char.codeUnitAt(0);
  int pos0 = seed % length;
  int pos = (pos0 + offset) % length;

  for (int i = 0; i < length - reserved; i++) {
    int i2 = (pos0 + reserved + i) % length;
    int c = input.codeUnitAt(i2);
    if (c >= start_char_code && c < (start_char_code + char_num)) {
      return input;
    }
  }

  return replace_char(input, pos, seed, char_num, start_char_code);
}
