import 'package:vanphash/vanphash.dart';
import 'package:vanphash/src/types.dart';
import 'package:vanphash/src/utils.dart';
import 'package:vanphash/src/models.dart';
import 'package:vanphash/src/rules.dart';
import 'package:vanphash/src/exceptions.dart';
import 'package:vanphash/src/hasher.dart';

import 'package:test/test.dart';

void main() {
  group('Password Model Tests:', () {
    String password;
    Password pass_model;

    setUp(() {
      password = "01se45/78Q";
      pass_model = Password(password);
    });

    test('basics', () {
      expect(password, equals(pass_model.password()));
      expect(password.length, equals(pass_model.length()));
    });

    test('character operations', () {
      var chars = pass_model.get_chars(CharType.digit, with_index: true);
      chars += pass_model.get_chars(CharType.lower_alpha, with_index: true);
      chars += pass_model.get_chars(CharType.upper_alpha, with_index: true);
      chars += pass_model.get_chars(CharType.special, with_index: true);

      expect(password.length, equals(chars.length));

      chars = pass_model.get_chars(CharType.digit, with_index: true);
      for (int i = 0; i < chars.length; i++) {
        expect(password[chars[i][0]], equals(chars[i][1]));
        expect(
            is_char_type(password[chars[i][0]], CharType.digit), equals(true));
      }

      chars = pass_model.get_chars(CharType.lower_alpha, with_index: true);
      for (int i = 0; i < chars.length; i++) {
        expect(password[chars[i][0]], equals(chars[i][1]));
        expect(is_char_type(password[chars[i][0]], CharType.lower_alpha),
            equals(true));
      }

      chars = pass_model.get_chars(CharType.upper_alpha, with_index: true);
      for (int i = 0; i < chars.length; i++) {
        expect(password[chars[i][0]], equals(chars[i][1]));
        expect(is_char_type(password[chars[i][0]], CharType.upper_alpha),
            equals(true));
      }

      chars = pass_model.get_chars(CharType.special, with_index: true);
      for (int i = 0; i < chars.length; i++) {
        expect(password[chars[i][0]], equals(chars[i][1]));
        expect(is_char_type(password[chars[i][0]], CharType.special),
            equals(true));
      }
    });
  });

  group('Password Rules Tests:', () {
    String password;

    setUp(() {
      password = "1234567890";
    });

    test('quantity rules', () {
      QuantityRule rule = QuantityRule(password);
      rule.size(8);
      expect(rule.get_password().length(), equals(8));
      expect(rule.get_password().password(), equals(password.substring(0, 8)));

      rule.min_chars(2, CharType.digit);
      expect(rule.get_password().count_chars(CharType.digit),
          greaterThanOrEqualTo(2));

      rule.min_chars(2, CharType.lower_alpha);
      expect(rule.get_password().count_chars(CharType.lower_alpha),
          greaterThanOrEqualTo(2));

      rule.min_chars(2, CharType.upper_alpha);
      expect(rule.get_password().count_chars(CharType.upper_alpha),
          greaterThanOrEqualTo(2));

      rule.min_chars(2, CharType.special);
      expect(rule.get_password().count_chars(CharType.special),
          greaterThanOrEqualTo(2));

      expect(() => rule.min_chars(5, CharType.special),
          throwsA(TypeMatcher<IllegalRule>()));
    });
  });

  group('Hashers Tests:', () {
    String tag;
    String key;
    int length;

    setUp(() {
      tag = "tag";
      key = "key";
      length = 15;
    });

    test('uid', () {
      Hasher hasher = Hasher.uid(tag: tag, key: key);
      expect(hasher.hash(), equals("INf7ygsX-I0RL-2Yq4-INf7-ygsXI0RL2Yq4"));
    });

    test('custom', () {
      Hasher hasher = Hasher.custom(tag, key, length, 2, 2, 2, 2);
      expect(hasher.hash(), equals("6+e+V88NesObMMr"));
    });

    test('vanphash', () {
      Hasher hasher = Hasher.vanphash(tag, key);
      expect(hasher.hash(), equals("6+(+V88NesObMMr"));
    });

    test('twik legacy', () {
      Hasher hasher = Hasher.twik(tag, key, length, PasswordType.numeric);
      expect(hasher.hash(), equals("641368861598774"));
      hasher = Hasher.twik(tag, key, length, PasswordType.alphanumeric);
      expect(hasher.hash(), equals("6weCV88NesObMMr"));
      hasher = Hasher.twik(tag, key, length, PasswordType.special);
      expect(hasher.hash(), equals("6we+V88NesObMMr"));
    });
  });

  group('Password Generation Tests:', () {
    String tag;
    String master_password;
    String secret_key;

    setUp(() {
      tag = 'tag';
      master_password = 'master-password';
      secret_key = 'secret-key';
    });

    test('legacy twik', () {
      String hashed_password = new_twik_password(
          tag, master_password, secret_key, 15, PasswordType.special);
      expect(hashed_password, equals("Xydq/CsifOveYu3"));
      expect(hashed_password.length, equals(15));
      hashed_password = new_twik_password(
          tag, master_password, secret_key, 15, PasswordType.numeric);
      expect(hashed_password, equals("143690885214203"));
      expect(hashed_password.length, equals(15));
      hashed_password = new_twik_password(
          tag, master_password, secret_key, 15, PasswordType.alphanumeric);
      expect(hashed_password, equals("XydqtCsifOveYu3"));
      expect(hashed_password.length, equals(15));
    });

    test('custom', () {
      String hashed_password =
          new_custom_password(tag, master_password, secret_key, 15, 2, 2, 2, 2);
      Password pass_obj = Password(hashed_password);
      expect(hashed_password, equals("d!S+aznP69a035O"));
      expect(hashed_password.length, equals(15));
      expect(pass_obj.count_chars(CharType.digit), greaterThanOrEqualTo(2));
      expect(
          pass_obj.count_chars(CharType.lower_alpha), greaterThanOrEqualTo(2));
      expect(
          pass_obj.count_chars(CharType.upper_alpha), greaterThanOrEqualTo(2));
      expect(pass_obj.count_chars(CharType.special), greaterThanOrEqualTo(2));
    });

    test('vanphash', () {
      String hashed_password =
          new_vanphash_password(tag, master_password, secret_key);
      Password pass_obj = Password(hashed_password);
      expect(hashed_password, equals("v21!1\',wINyVqqG"));
      expect(hashed_password.length, equals(15));
      expect(pass_obj.count_chars(CharType.digit), greaterThanOrEqualTo(3));
      expect(
          pass_obj.count_chars(CharType.lower_alpha), greaterThanOrEqualTo(3));
      expect(
          pass_obj.count_chars(CharType.upper_alpha), greaterThanOrEqualTo(3));
      expect(pass_obj.count_chars(CharType.special), greaterThanOrEqualTo(3));
    });
  });
}
