enum PasswordType {
  /// Twik password types enum.
  numeric,
  alphanumeric,
  special
}

enum CharType {
  /// Character types enum.
  digit,
  special,
  lower_alpha,
  upper_alpha
}
