[![pipeline status](https://gitlab.com/orishiku/vanphash_dart/badges/develop/pipeline.svg)](https://gitlab.com/orishiku/vanphash_dart/-/commits/develop) [![coverage report](https://gitlab.com/orishiku/vanphash_dart/badges/develop/coverage.svg)](https://gitlab.com/orishiku/vanphash_dart/-/commits/develop)


Vanilla Password Hashing or vanphash is a simple password generator library based on hashing,
it starts as a legacy alternative to Twik.

[license](https://gitlab.com/orishiku/vanphash_dart/-/raw/develop/LICENSE).

## Usage

A simple usage example:

```dart
import 'package:vanphash/vanphash.dart';
import 'package:vanphash/src/types.dart';

main() {
	String tag = 'tag';
	String master_password = 'master_password';
	String secret_key = 'secret_key';

	print(twik_password(tag, master_password, secret_key,15,
	PasswordType.special));
	print(custom_password(tag, master_password, secret_key,15, 3, 3, 3 ,3));
	print(vanphash_password(tag, master_password, secret_key));
}
```

## Features and bugs
This package is still a beta, further version won't generate passwords compatible with previous versions.

The only feature that won't change its generated passwords is `twik_password(...)` since it replicates the android applicacion algorithm.