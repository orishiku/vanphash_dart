## 2020.0.4
- added comments to classes and methods.
- added private key generation

## 2020.0.3
- Better internal hashers management.
- added minimal character count by type.

## 2020.0.2
## 2020.0.1-1
## 2020.0.1
- Fixes for pub package

## 2020.0.0
- Initial version, created by Stagehand
- Password hashing
- Twik compatible password generation
- Incomplete vanphash password generation
- Incomplete custom password generation

## 0.3.1
