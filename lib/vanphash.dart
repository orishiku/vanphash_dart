/// Vanilla Password Hashing or vanphash is a simple password generator based on hashing, it starts as a legacy alternative to Twik Android app.
library vanphash;

import 'package:vanphash/src/hasher.dart';
import 'package:vanphash/src/types.dart';

new_secret_key({String tag = '', String key = ''}) {
  /// Generates a new secret key usefull to work with password generators.
  return Hasher.uid(tag: tag, key: key).hash();
}

new_twik_password(String tag, String master_password, String secret_key,
    int length, PasswordType password_type) {
  /// Password generator with legacy security directives compatible with Twik android app
  if (master_password == '') {
    return '';
  }
  if (secret_key != null) {
    tag = Hasher.twik(secret_key, tag, 24, PasswordType.special).hash();
  }

  return Hasher.twik(tag, master_password, length, password_type).hash();
}

new_custom_password(
    String tag,
    String master_password,
    String secret_key,
    int length,
    int min_upper_alphas,
    int min_lower_alphas,
    int min_digits,
    int min_specials) {
  /// Password generator with custom security directives
  if (master_password == '') {
    return '';
  }
  if (secret_key != null) {
    tag = Hasher.custom(secret_key, tag, 24, min_upper_alphas, min_lower_alphas,
            min_digits, min_specials)
        .hash();
  }

  return Hasher.custom(tag, master_password, length, min_upper_alphas,
          min_lower_alphas, min_digits, min_specials)
      .hash();
}

new_vanphash_password(String tag, String master_password, String secret_key) {
  /// Password generator with vanphash security directives
  if (master_password == '') {
    return '';
  }
  if (secret_key != null) {
    tag = Hasher.vanphash(secret_key, tag).hash();
  }

  return Hasher.vanphash(tag, master_password).hash();
}
