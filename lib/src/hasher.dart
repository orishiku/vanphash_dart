import 'dart:convert';
import 'dart:io' show Platform;
import 'package:crypto/crypto.dart';

import 'package:vanphash/src/types.dart';
import 'package:vanphash/src/utils.dart';
import 'package:vanphash/src/rules.dart';

class Hasher {
  /// Main hasher object
  String _hash;

  _hasher(String tag, String key, int length) {
    /// Base hashing method
    List<int> tag_bytes = utf8.encode(tag);
    List<int> key_bytes = utf8.encode(key);

    var hmacSha1 = Hmac(sha1, key_bytes);
    var digest = hmacSha1.convert(tag_bytes);

    this._hash = base64.encode(digest.bytes);
  }

  Hasher.vanphash(String tag, String key) {
    /// Hasher constructor with vanphash directives.
    this._hasher(tag, key, 15);

    QuantityRule rule = QuantityRule(this._hash);
    rule.size(15);
    rule.min_chars(3, CharType.digit);
    rule.min_chars(3, CharType.lower_alpha);
    rule.min_chars(3, CharType.upper_alpha);
    rule.min_chars(3, CharType.special);

    this._hash = rule.get_password().password();
  }

  Hasher.custom(String tag, String key, int length, int min_digits,
      int min_lower_alphas, int min_upper_alphas, int min_specials) {
    /// Hasher constructor with custom directives.
    this._hasher(tag, key, length);

    QuantityRule rule = QuantityRule(this._hash);
    rule.size(length);
    rule.min_chars(min_digits, CharType.digit);
    rule.min_chars(min_lower_alphas, CharType.lower_alpha);
    rule.min_chars(min_upper_alphas, CharType.upper_alpha);
    rule.min_chars(min_specials, CharType.special);

    this._hash = rule.get_password().password();
  }

  hash() {
    return this._hash;
  }

  Hasher.twik(String tag, String key, int length, PasswordType type) {
    /// Hasher constructor with twik legacy directives.
    this._hasher(tag, key, length);

    this._hash = this._hash.substring(0, this._hash.length - 1);

    int sum = 0;
    for (int i = 0; i < this._hash.length; i++) {
      sum += this._hash.codeUnitAt(i);
    }

    if (type == PasswordType.numeric) {
      this._hash = convert_to_digits(this._hash, sum, length);
    } else {
      this._hash = random_inject_character(
          this._hash, 0, 4, sum, length, CharType.digit);
      if (type == PasswordType.special) {
        this._hash = random_inject_character(
            this._hash, 1, 4, sum, length, CharType.special);
      }

      this._hash = random_inject_character(
          this._hash, 2, 4, sum, length, CharType.upper_alpha);
      this._hash = random_inject_character(
          this._hash, 3, 4, sum, length, CharType.lower_alpha);

      if (type == PasswordType.alphanumeric) {
        this._hash = remove_special_characters(this._hash, sum, length);
      }
    }

    this._hash = this._hash.substring(0, length);
  }

  Hasher.uid({String tag = '', String key = ''}) {
    /// Hasher constructor for uid generation.
    tag = tag != '' ? tag : DateTime.now().toIso8601String();
    key = key != '' ? tag : Platform.operatingSystem;

    this._hasher(tag, key, 16);
    String hash_a = remove_special_characters(this._hash, 32, 16);
    hash_a = (hash_a.substring(0, 8) +
        '-' +
        hash_a.substring(8, 12) +
        '-' +
        hash_a.substring(12, 16));

    this._hasher(key, tag, 16);
    String hash_b = remove_special_characters(this._hash, 32, 16);
    hash_b = (hash_b.substring(0, 4) + '-' + hash_b.substring(4, 16));

    this._hash = hash_a + '-' + hash_b;
  }
}
