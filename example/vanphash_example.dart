import 'package:vanphash/vanphash.dart';
import 'package:vanphash/src/types.dart';


main() {
  String tag = 'tag';
  String master_password = 'master_password';
  String secret_key = new_secret_key();

  print(new_twik_password(
      tag, master_password, secret_key, 15, PasswordType.special));
  print(new_custom_password(tag, master_password, secret_key, 15, 3, 3, 3, 3));
  print(new_vanphash_password(tag, master_password, secret_key));
}
